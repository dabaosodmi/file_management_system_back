package com.captain.service.impl;

import com.captain.entity.po.Employ;
import com.captain.mapper.EmployMapper;
import com.captain.service.EmployService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
@Service
public class EmployServiceImpl extends ServiceImpl<EmployMapper, Employ> implements EmployService {

}
