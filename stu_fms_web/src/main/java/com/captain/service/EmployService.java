package com.captain.service;

import com.captain.entity.po.Employ;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
public interface EmployService extends IService<Employ> {

}
