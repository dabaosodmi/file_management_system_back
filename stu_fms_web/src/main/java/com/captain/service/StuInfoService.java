package com.captain.service;

import com.captain.entity.po.StuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lianhong
 * @since 2020-08-26
 */
public interface StuInfoService extends IService<StuInfo> {

}
