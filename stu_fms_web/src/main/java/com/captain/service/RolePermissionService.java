package com.captain.service;

import com.captain.entity.po.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
public interface RolePermissionService extends IService<RolePermission> {

}
