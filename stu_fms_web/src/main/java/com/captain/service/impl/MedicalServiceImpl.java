package com.captain.service.impl;

import com.captain.entity.po.Medical;
import com.captain.mapper.MedicalMapper;
import com.captain.service.MedicalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lianhong
 * @since 2020-08-24
 */
@Service
public class MedicalServiceImpl extends ServiceImpl<MedicalMapper, Medical> implements MedicalService {

}
