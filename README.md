# 学生档案管理系统需求分析



## 角色划分

1. 学生

2. 档案管理员

3. 相关部门人员(教务处人员、医务人员、学生处人员)

4. 系统管理员

   使用shiro对资源进行权限划分，便于随时修改用户权限或者添加角色

## 功能模块划分

### 学生档案

1. 基本信息
   1. 创建
   2. 编辑
   3. 提交审核
   4. 查看
2. 课程学业信息
   1. 创建
   2. 编辑
   3. 提交审核
   4. 查看
3. 活动/竞赛信息
   1. 添加
   2. 编辑
   3. 提交审核
   4. 查看
4. 健康体检信息
   1. 查看
   2. 添加
   3. 编辑
   4. 提交审核
5. 违纪信息
   1. 查看
   2. 添加
   3. 撤销
   4. 编辑
   5. 提交审核
6. 就业与发展信息
   1. 查看
   2. 添加
   3. 编辑
   4. 提交审核

### 档案管理

1. 档案归档
   1. 归档审核
   2. 归档建目
   3. 提交入库
2. 档案入库
   1. 新增
   2. 修改
   3. 删除
   4. 查询
   5. 数据统计
3. 档案出库(出库自动销毁)
   1. 时效性检查（显示超过保存时间的	）
   2. 档案出库
4. 档案借阅
   1. 借阅审批(在线借阅或者下载，只能在规定时间内查看或者下载,超时自动结束)
   2. 借阅归还
   3. 生成借阅报表

### 档案查询借阅模块

1. 查询档案
   1. 查询是否审核通过
   2. 是否入库
2. 申请借阅档案(有时间限制)
3. 下载档案(需要生成报表)

### 系统管理模块

1. 用户管理
   1. 批量导入
   2. 批量导出
   3. 新增
   4. 删除
   5. 修改
   6. 多条件查询
2. 角色管理
   1. 新增角色
   2. 删除角色
   3. 修改角色
3. 权限管理
   1. 新增
   2. 删除
   3. 修改
   4. 查询(采用树形组件，不单独设置多条件查询)
4. 数据管理
5. 系统设置(配色、版本、开发者信息、版权声明、网站名称、备案信息)

### 登录注册模块

1. 登录
2. 注销登录
3. 记住密码

### 个人设置模块

1. 用户信息(修改密码、头像、昵称)
2. 学生基本信息(与学生档案信息数据同步)

## 数据库设计

1. 用户表(user)

   | 名称 | 字段名   | 类型        | 是否主键 |
   | ---- | -------- | ----------- | -------- |
   | id   | id       | bigint      | 是       |
   | 账户 | account  | varchar(20) |          |
   | 密码 | password | varchar(50) |          |

2. 角色表(role)

   | 名称     | 字段名 | 类型         | 是否主键 |
   | -------- | ------ | ------------ | -------- |
   | id       | id     | bigint       | 是       |
   | 角色名称 | name   | varchar(20)  |          |
   | 角色描述 | desc   | varchar(200) |          |

3. 权限表(perm)

   | 名称     | 字段名 | 类型         | 是否主键 |
   | -------- | ------ | ------------ | -------- |
   | id       | id     | bigint       | 是       |
   | 简称     | name   | varchar(20)  |          |
   | 描述     | desc   | varchar(255) |          |
   | 资源路径 | url    | varchar(255) |          |
   | 权限类型 | type   | int          |          |
   | 权限名   | perms  | varchar(200) |          |

4. 角色权限表(role_perm)

   | 名称   | 字段名  | 类型   | 是否主键 |
   | ------ | ------- | ------ | -------- |
   | id     | id      | bigint | 是       |
   | 角色ID | role_id | bigint |          |
   | 权限ID | perm_id | bigint |          |

5. 用户角色表(user_role)

   | 名称   | 字段名  | 类型   | 是否主键 |
   | ------ | ------- | ------ | -------- |
   | id     | id      | bigint | 是       |
   | 角色ID | role_id | bigint |          |
   | 用户ID | user_id | bigint |          |

6. 个人基本信息表(base_info)

   | 名称         | 字段名      | 类型         | 是否主键 |
   | ------------ | ----------- | ------------ | -------- |
   | id           | id          | bigint       | 是       |
   | 审核状态ID   | verify_id   | bigint       |          |
   | 用户ID       | user_id     | bigint       |          |
   | 学号         | stu_account | varchar(20)  |          |
   | 姓名         | stu_name    | varchar(20)  |          |
   | 籍贯         | stu_home    | varchar(120) |          |
   | 政治面貌     | political   | int          |          |
   | 入学时间     | in_time     | datetime     |          |
   | 毕业时间     | out_time    | datetime     |          |
   | 所属学院名称 | coll_name   | int          |          |
   | 院系名称     | series_name | int          |          |
   | 专业名称     | prof_name   | int          |          |
   | 行政班级     | class       | int          |          |
   | 毕业学历     | academic    | varchar(20)  |          |
   | 毕业学位     | degree      | varchar(20)  |          |
   | 电话         | tel         | varchar(11)  |          |
   | 邮件         | email       | varchar(20)  |          |

7. 课业成绩表(course)

   | 字段名    | 类型        | 是否主键 |   名称   |
   | --------- | ----------- | -------- | :------: |
   | id        | bigint      | 是       |    id    |
   | verify_id | bigint      |          | 审核状态 |
   | user_id   | bigint      |          |  用户ID  |
   | name      | bigint      |          | 课程名称 |
   | score     | int         |          | 课程成绩 |
   | rank      | int         |          | 课程排名 |
   | teacher   | varchar(20) |          | 任课老师 |

   

8. 科研项目表(project)

   | 名称       | 字段名    | 类型        | 是否主键 |
   | ---------- | --------- | ----------- | -------- |
   | id         | id        | bigint      | 是       |
   | 用户ID     | user_id   | bigint      |          |
   | 审核状态ID | verify_id | bigint      |          |
   | 项目名称   | name      | varchar(20) |          |
   | 项目类型   | type      | int         |          |
   | 项目经费   | funds     | float       |          |
   | 项目状态   | status    | int         |          |
   | 项目简介   | info      | text        |          |

   

9. 参与活动/竞赛表(activity)

   | 名称          | 字段名    | 类型         | 是否主键 | 备注                                         |
   | ------------- | --------- | ------------ | -------- | -------------------------------------------- |
   | id            | id        | bigint       | 是       |                                              |
   | 用户ID        | user_id   | bigint       |          |                                              |
   | 审核状态ID    | verify_id | bigint       |          |                                              |
   | 活动/竞赛名称 | name      | varchar(20)  |          |                                              |
   | 活动/竞赛等级 | level     | varchar(20)  |          |                                              |
   | 活动/竞赛类型 | type      | int          |          | 分为：学术竞赛、体育活动、校园活动、道德奖励 |
   | 参与/获得时间 | time      | datetime     |          |                                              |
   | 参与/获得证明 | img       | varchar(100) |          |                                              |

10. 体育体检表(medical)

    | 名称         | 字段名    | 类型         | 是否主键 |
    | ------------ | --------- | ------------ | -------- |
    | id           | id        | bigint       | 是       |
    | 用户ID       | user_id   | bigint       |          |
    | 审核状态ID   | verify_id | bigint       |          |
    | 体检时间     | time      | datetime     |          |
    | 体检结果说明 | desc      | text         |          |
    | 体检结果图片 | img       | varchar(100) |          |

11. 违纪信息表(discipline)

    | 名称       | 字段名    | 类型        | 是否主键 |
    | ---------- | --------- | ----------- | -------- |
    | id         | id        | bigint      | 是       |
    | 用户ID     | user_id   | bigint      |          |
    | 审核状态ID | verify_id | bigint      |          |
    | 违纪名称   | name      | varchar(20) |          |
    | 违纪时间   | time      | datetime    |          |
    | 违纪说明   | desc      | text        |          |

    

12. 就业发展信息表(grow_info)

    | 名称         | 字段名    | 类型        | 是否主键 |
    | ------------ | --------- | ----------- | -------- |
    | id           | id        | bigint      | 是       |
    | 用户ID       | user_id   | bigint      |          |
    | 审核状态ID   | verify_id | bigint      |          |
    | 毕业工作时间 | work_time | datetime    |          |
    | 就职单位     | depart    | varchar(50) |          |
    | 任职情况     | desc      | textss      |          |

    

13. 档案审核状态类型表（verify_status）

    | 名称     | 字段名 | 类型        | 是否主键 |
    | -------- | ------ | ----------- | -------- |
    | id       | id     | bigint      | 是       |
    | 状态名称 | name   | varchar(20) |          |
    | 状态描述 | desc   | text        |          |

    

14. 档案信息表(archive)

    | 名称             | 字段名       | 类型     | 是否主键 |
    | ---------------- | ------------ | -------- | -------- |
    | id               | id           | bigint   | 是       |
    | 用户ID           | user_id      | bigint   |          |
    | 档案创建时间     | created      | datetime |          |
    | 借阅次数         | borr_count   | int      |          |
    | 档案保存截止时间 | cut_off_time | datetime |          |

15. 档案借阅记录表(archive_borr)

|     名称     |    字段名    |   类型   | 是否主键 |
| :----------: | :----------: | :------: | :------: |
|      id      |      id      |  bigint  |    是    |
| 开始借阅时间 |  borr_time   | datetime |          |
| 被借阅用户ID |   user_id    |  bigint  |          |
|  借阅用户ID  | borr_user_id |  bigint  |          |
|   阅览时长   |  view_time   | datetime |          |
|  是否已审批  |   verifyed   | tinyint  |          |