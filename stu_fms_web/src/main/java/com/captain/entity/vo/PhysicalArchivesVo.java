package com.captain.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PhysicalArchivesVo {

    //id
    private Long id;

    //学号
    private String stuId;


}
