package com.captain.service.impl;

import com.captain.entity.po.RolePermission;
import com.captain.mapper.RolePermissionMapper;
import com.captain.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
