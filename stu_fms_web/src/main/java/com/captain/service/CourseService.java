package com.captain.service;

import com.captain.entity.po.Course;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lianhong
 * @since 2020-08-24
 */
public interface CourseService extends IService<Course> {

}
