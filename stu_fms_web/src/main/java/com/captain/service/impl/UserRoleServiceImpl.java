package com.captain.service.impl;

import com.captain.entity.po.UserRole;
import com.captain.mapper.UserRoleMapper;
import com.captain.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
