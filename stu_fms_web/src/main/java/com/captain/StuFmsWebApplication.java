package com.captain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StuFmsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(StuFmsWebApplication.class, args);
    }

}
