package com.captain.service.impl;

import com.captain.entity.po.Archive;
import com.captain.mapper.ArchiveMapper;
import com.captain.service.ArchiveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxb
 * @since 2020-08-25
 */
@Service
public class ArchiveServiceImpl extends ServiceImpl<ArchiveMapper, Archive> implements ArchiveService {

}
