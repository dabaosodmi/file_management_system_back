package com.captain.service.impl;

import com.captain.entity.po.Discipline;
import com.captain.mapper.DisciplineMapper;
import com.captain.service.DisciplineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lianhong
 * @since 2020-08-24
 */
@Service
public class DisciplineServiceImpl extends ServiceImpl<DisciplineMapper, Discipline> implements DisciplineService {

}
