package com.captain.mapper;

import com.captain.entity.po.Archive;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxb
 * @since 2020-08-25
 */
public interface ArchiveMapper extends BaseMapper<Archive> {

}
