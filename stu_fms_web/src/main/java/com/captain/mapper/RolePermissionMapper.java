package com.captain.mapper;

import com.captain.entity.po.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
