package com.captain.service;

import com.captain.entity.po.Project;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lianhong
 * @since 2020-08-24
 */
public interface ProjectService extends IService<Project> {

}
