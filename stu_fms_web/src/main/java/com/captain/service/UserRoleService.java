package com.captain.service;

import com.captain.entity.po.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
public interface UserRoleService extends IService<UserRole> {

}
