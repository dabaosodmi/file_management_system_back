package com.captain.mapper;

import com.captain.entity.po.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lll
 * @since 2020-08-28
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
