package com.captain.mapper;

import com.captain.entity.po.StuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lianhong
 * @since 2020-08-26
 */
public interface StuInfoMapper extends BaseMapper<StuInfo> {

}
