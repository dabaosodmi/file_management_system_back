package com.captain.mapper;

import com.captain.entity.po.Activity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lianhong
 * @since 2020-08-24
 */
public interface ActivityMapper extends BaseMapper<Activity> {

}
