package com.captain.service;

import com.captain.entity.po.Archive;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxb
 * @since 2020-08-25
 */
public interface ArchiveService extends IService<Archive> {

}
