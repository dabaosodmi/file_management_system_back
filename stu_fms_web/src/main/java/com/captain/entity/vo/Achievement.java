package com.captain.entity.vo;

import lombok.Data;

/**
 * 个人成就对象，用于Apriori分析
 */
@Data
public class Achievement extends AprioriElement {

}
